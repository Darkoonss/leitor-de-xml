<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        
        <!-- Data Tables -->
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
        <style>
            #tabela th, #tabela td {
                word-break: break-all;
                word-break: break-word;
            }
        </style>

    </head>
    <body class="antialiased">
        <div class="container">
            <a href="{{ route('Welcome.Index') }}">
                <h1 class="mt-4">Leitor de XML</h1>
            </a>
            <hr>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            {{-- Inicio Formulario Envio --}}
                <div class="row">
                    <form action="{{ route('Welcome.Store') }}" method="POST" enctype="multipart/form-data" class="form-inline">
                        @csrf

                        <div class="col-6 form-group">
                            <label for="inputFile">XML:</label>
                            <input type="file" class="form-control-file" id="inputFile" name="file" accept=".xml">
                            <small id="inputFile" class="form-text text-muted">É aceito apenas arquivo XML.</small>
                        </div>
                        <div class="col-2 form-group">
                            <button type="submit" class="btn btn-primary mb-2">Enviar</button>
                        </div>
                    </form>
                </div>
            {{-- Fim Formulario Envio --}}

            {{-- Inicio Tabela --}}
                @if(session('Registros'))
                    <div class="row mt-4">
                        <table id="tabela" class="table table-striped">
                            <thead>
                                <tr>
                                    <th width="50%">Caminho</th>
                                    <th>Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(session('Registros') as $caminho => $valor)
                                    <tr>
                                        <th>{{ $caminho }}</th>
                                        <td>{{ $valor }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            {{-- Inicio Tabela --}}
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>

        <script>
            $('#tabela').DataTable({
                "responsive": true
            });
        </script>
    
    </body>
</html>
