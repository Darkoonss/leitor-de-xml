<?php

namespace App\Http\Requests\Welcome;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file'  =>  'required|mimes:application/xml,xml|max:3000'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'file.required' => 'É necessário enviar o arquivo XML.',
            'file.mimes'    => 'É necessário que o arquivo enviado seja um XML.',
            'file.size'     => 'É necessário que o arquivo tenha no maximo :size kilobytes ',
        ];
    }
}
