<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\Welcome\PostRequest;

class WelcomeController extends Controller
{
    /** Função retorna o usuário para a view Welcome
     * @date 21-10-2020
     * @return view
     */
    public function index(){
            return view('welcome');
    }

    /** Função gerencia o fluxo de obtenção do processamento do xml
     * @date 22-10-2020
     * @param PostRequest request
     * @return back
     */
    public function store(PostRequest $request){
        $xml = simplexml_load_file($request->file->path());

        $retorno = $this->processarXML($xml);

        $request->session()->flash('Registros', $retorno);
        return back();
    }

    /** Função processa de forma recursiva os itens presentes em um xml
     * @date 21-10-2020
     * @param string colecao
     * @return array
     */
    private function processarXML($colecao,$percurso = [],$retorno = []){
        $percurso[] = $colecao->getName();
        
        $filhos = $colecao->children();
        if ($filhos->count() == 0)  {
            $caminho = implode("/", $percurso);
            $retorno[ $caminho ] = (string) $colecao;
        }else{
            foreach ($filhos as $filho){
                $retorno = $this->processarXML($filho,$percurso,$retorno);
            }
        }

        return $retorno;
    }
}
