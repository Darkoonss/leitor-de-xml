# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |vagrant|

  vagrant.vm.box = "ubuntu/bionic64"

  vagrant.vm.synced_folder ".", "/var/www/html"
 
  vagrant.vm.network "forwarded_port", guest: 80, host: 80

  ## Virtualbox
  vagrant.vm.provider "virtualbox" do |v|
    v.memory = 512
    v.cpus = 2
  end

  ## Atualização
  vagrant.vm.provision "shell" do |s|
      s.inline = "sudo apt update && sudo apt upgrade -y"
  end

  ## Provision Apache
  vagrant.vm.provision "apache2", type: "shell", inline: <<-SHELL
    sudo apt install apache2 -y
    sudo rm /etc/apache2/sites-available/000-default.conf
    sudo cp /var/www/html/vagrant/apache2/000-default.conf /etc/apache2/sites-available/000-default.conf 
    cd /etc/apache2/mods-enabled/
    sudo a2enmod rewrite
    sudo service apache2 restart
  SHELL

  ## Provision php
  vagrant.vm.provision "php", type: "shell", inline: <<-SHELL
      cd / 
      sudo apt-get install software-properties-common
      sudo add-apt-repository ppa:ondrej/php -y
      sudo apt install php7.4 unzip php7.4-zip php7.4-common php7.4-mbstring php7.4-dom php7.4-opcache php7.4-cli php7.4-gd php7.4-curl php7.4-mysql -y
  SHELL

  ## Provision composer
  vagrant.vm.provision "composer", type: "shell", inline: <<-SHELL
    cd /var/www/html/
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php -r "if (hash_file('sha384', 'composer-setup.php') === 'c31c1e292ad7be5f49291169c0ac8f683499edddcfd4e42232982d0fd193004208a58ff6f353fde0012d35fdd72bc394') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    php composer-setup.php
    php -r "unlink('composer-setup.php');"

    cd /
    sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
    sudo /sbin/mkswap /var/swap.1
    sudo /sbin/swapon /var/swap.1
  SHELL

  ## Provision laravel
  vagrant.vm.provision "laravel", type: "shell", inline: <<-SHELL
    cd /var/www/html/
    cp .env.example .env
    sudo php composer.phar install
    sudo php artisan key:generate
    sudo php artisan storage:link
    sudo chmod -R 777 /var/www/html/
  SHELL

end
